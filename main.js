let http = require('http');
let moment = require('moment');
let arr = [];

let request = http.get('http://stream.meetup.com/2/rsvps?order=time', response => {

    response.on('data', (data) => {
        let buff = Buffer.from(data);
        try { arr.push(JSON.parse(buff)); } catch (e) {}
    });

    response.on('end', () => {
        let countryMap = {};

        arr.reduce((acc, obj) => {
            let key = obj.group.group_country;

            if (!acc.hasOwnProperty(key)) {
                acc[key] = 1;
            } else {
                acc[key]++;
            }
            return acc;
        }, countryMap);

        let sortedByCount = Object.keys(countryMap).sort((a,b) => { return countryMap[b] - countryMap[a] });
        let result = `${arr.length},${moment(arr[arr.length -1].mtime).format('YYYY-MM-DD HH:mm')},${arr[arr.length -1].event.event_url}`;
        for(let i = 0; i < 3; i++) {
            if(sortedByCount[i]) {
                result += `,${sortedByCount[i]},${countryMap[sortedByCount[i]]}`;
            } else {
                break;
            }
        }
        console.log(result);
    });
});

setTimeout(() => {
    request.abort();
}, 60000);
